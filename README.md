# soundcloud-dl

![](https://img.shields.io/badge/written%20in-PHP-blue)

A music and metadata downloader for the web service soundcloud.com.

## Features

- Find tracks by user or search
- Download files and metadata or output titles/URLs for further processing
- Filter results by duration and regular expression on title
- Suitable for shell and unattended cron use

## Usage

Type `./soundcloud-dl` to see full usage.

Tags: scraper


## Download

- [⬇️ soundcloud-dl_20131021.tar.gz](dist-archive/soundcloud-dl_20131021.tar.gz) *(2.98 KiB)*
